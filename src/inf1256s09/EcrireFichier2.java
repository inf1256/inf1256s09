/**
 * @author Johnny Tsheke @ UQAM
 * 2017-04-03
 */
package inf1256s09;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class EcrireFichier2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String separateur = System.lineSeparator();
		Scanner clavier = new Scanner(System.in).useDelimiter(separateur);
		Path fichier = Paths.get("src/fichier4.txt");//racine: dossier projet
		try(BufferedWriter writer = Files.newBufferedWriter(fichier);){
		     while(true){
		    	 System.out.println("entrez une chaine ou appuyez sur espace puis entree pour terminer");
		    	 String ligne=clavier.next().trim();//lecture 1ere ligne
		    	 if(ligne.isEmpty()||ligne.equalsIgnoreCase(separateur)){
		    	 	//si ligne vide ou caractere de fin de ligne seulement
		    		 break;
		    	 }
		    	 writer.write(ligne);
		    	 writer.newLine();
		     }
			}catch (IOException x){
			   System.out.println("Erreur lecture fichier");
			}
	}

}

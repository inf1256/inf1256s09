package inf1256s09;
import java.lang.*;
public class Chaines {

	public static void main(String[] args) {
		String chaine = "petite italie";
		System.out.println(chaine.charAt(2));
		//index dernière lettre t
		System.out.println(chaine.lastIndexOf("t"));
		//index première lettre i
	    System.out.println(chaine.indexOf("i"));
	    //index 1er i à partir de l'index 5
	    System.out.println(chaine.indexOf("i", 5));
	  //index 1er i à partir de l'index 5
	    System.out.println(chaine.indexOf("i", 16));//-1 -> existe pas
		//majuscule
		System.out.println(chaine.toUpperCase());
		//capitalisation 1ere lettre
		System.out.println(chaine.substring(0,1).toUpperCase().concat(chaine.substring(1))); 
	}
}

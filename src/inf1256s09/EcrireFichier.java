/**
 * @author Johnny Tsheke @ UQAM
 * 2017-04-03
 */
package inf1256s09;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class EcrireFichier {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String separateur = System.lineSeparator();
		Scanner clavier = new Scanner(System.in).useDelimiter(separateur);
		Path fichier = Paths.get("src/fichier2.txt");//racine: dossier projet
		try(BufferedWriter writer = Files.newBufferedWriter(fichier);){
		     while(true){
		    	 System.out.println("entrez une chaine ou appuyez sur espace puis entree pour terminer");
		    	 String ligne=clavier.nextLine().trim();//lecture 1ere ligne
		    	 if(ligne.isEmpty()){
		    		 break;
		    	 }
		    	 writer.write(ligne);
		    	 writer.newLine();
		     }
			}catch (IOException x){
			   System.out.println("Erreur lecture fichier");
			}
	}

}

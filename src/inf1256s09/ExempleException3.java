package inf1256s09;

import java.util.Locale;
import java.util.Scanner;

public class ExempleException3 {
    public static void main (String[] args){
        Scanner clavier = new Scanner(System.in);
        clavier.useLocale(Locale.US);
        int nombreEntier=0;
        System.out.println("Saisir une chaine ou 0 pour provoquer exception");
        try{ // ATT: en Java la division par 0 retourne Infinity et pas une Exception!
            nombreEntier = clavier.nextInt();
            if(nombreEntier == 0){//renvoyer exception
                throw new Exception("On ne peut pas diviser un nombre par 0");
            }
            System.out.println("Inverse de "+ nombreEntier +" = "+ 1.0/nombreEntier);
        }catch (Exception e){
            e.printStackTrace();//peut servir pour debogguer
            System.out.println("Veuillez saisir un nombre entier positif svp!");
        } finally {
            System.out.println("\n Erreurs sous controle!");
        }
    }
}

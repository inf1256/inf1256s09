package inf1256s09;

import java.util.Locale;
import java.util.Scanner;

public class ExempleException1 {
    public static void main (String[] args){
        Scanner clavier = new Scanner(System.in);
        clavier.useLocale(Locale.US);
        int nombreEntier=0;
        System.out.println("Saisir une chaine pour provoquer une exception");
        try{ //code a surveiller
            nombreEntier = clavier.nextInt();
            System.out.println("Inverse de "+ nombreEntier +" = "+ 1.0/nombreEntier);
            // ATT: en java la division par 0 retourne Infinity et pas une Exception!
        }catch (Exception e){ // si une exception se produit
            //e.printStackTrace();//peut servir à debogguer
            System.out.println("Veuillez saisir un nombre entier positif svp!");
        }
    }
}

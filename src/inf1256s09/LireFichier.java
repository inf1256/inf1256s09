/**
 * @author Johnny Tsheke @ UQAM
 * 2017-04-03
 */
package inf1256s09;

import java.io.*;
import java.nio.file.*;

public class LireFichier {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Path fichier = Paths.get("src/fichier1.txt");//dossier projet racine
		try(BufferedReader reader = Files.newBufferedReader(fichier);){
		     String ligne=reader.readLine();//lecture 1ere ligne
		     while(ligne!=null){
		    	 System.out.println(ligne);
		    	 ligne=reader.readLine();//ligne suivante
		     }
			}catch (IOException x){
			   System.out.println("Erreur lecture fichier");
			}
	}

}
